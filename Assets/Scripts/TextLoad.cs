﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLoad : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer spriteCollider;
    [SerializeField]
    private PolygonCollider2D polygonCollider;
    [SerializeField]
    private List<int> _ID;
    [SerializeField]
    private List<int> _allowedIDs = new List<int>();
    [SerializeField]
    private bool movable = true;
    [SerializeField]
    private bool rotatable = true;
    public List<int> ID { get => _ID; set => _ID = value; }
    public List<int> allowedIDs { get => _allowedIDs; set => _allowedIDs = value; }
    public void SetupText(Texture textTexture, Color textColor, Sprite spriteImage, List<int> ID, List<int> allowedIDs)
    {
        GetComponent<MeshRenderer>().material.mainTexture = textTexture;
        GetComponent<MeshRenderer>().material.SetTexture("_EmissionMap", textTexture);
        GetComponent<MeshRenderer>().material.SetColor("_EmissionColor",textColor);
        this.ID = ID;
        this.allowedIDs = allowedIDs;
        spriteCollider.sprite = spriteImage;
        for (int i = 0; i < polygonCollider.pathCount; i++) 
            polygonCollider.SetPath(i, new List<Vector2>());
        polygonCollider.pathCount = spriteCollider.sprite.GetPhysicsShapeCount();

        List<Vector2> path = new List<Vector2>();
        for (int i = 0; i < polygonCollider.pathCount; i++)
        {
            path.Clear();
            spriteCollider.sprite.GetPhysicsShape(i, path);
            polygonCollider.SetPath(i, path.ToArray());
        }
    }

    public bool IsMovable()
    {
        return movable;
    }
}
