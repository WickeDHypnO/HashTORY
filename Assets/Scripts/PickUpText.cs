﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using TMPro;

public class PickUpText : MonoBehaviour
{
    private RaycastHit2D hit = default;
    private GameObject pickedText = default;
    private Vector2 offset;
    private Vector2 mousePositionAtStart;
    private List<TextLoad> texts;
    [SerializeField]
    private float minimumDistanceToSnap = 0.01f;
    public bool changeLevel = false;
    private float timer = 0f;
    private bool timerStarted = false;
    [SerializeField]
    private float rotationDelayMax = 0.2f;
    public CanvasGroup textExplanationGroup = default;

    private bool canInteract = true;
    [SerializeField]
    private AudioClip clickSound;
    [SerializeField]
    private AudioClip stickSound;
    [SerializeField]
    private AudioClip winSound;

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !changeLevel && canInteract)
        {
            hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));
            if (hit.collider != null && hit.collider.GetComponentInParent<TextLoad>() && pickedText == null && hit.collider.GetComponentInParent<TextLoad>().IsMovable())
            {
                pickedText = hit.collider.GetComponentInParent<TextLoad>().gameObject;
                offset = (Vector2)hit.collider.transform.position - hit.point;
                timerStarted = true;
            }
        }
        else if (Input.GetMouseButtonUp(0) && pickedText && canInteract)
        {
            if (timer < rotationDelayMax && pickedText)
            {
                canInteract = false;
                hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));
                var rotGO = new GameObject("RotateHelper");
                rotGO.transform.position = hit.point;
                pickedText.transform.parent = rotGO.transform;
                var rotatingText = pickedText;
                rotGO.transform.DORotate(new Vector3(0, 0, -90), 0.2f, RotateMode.WorldAxisAdd).OnComplete(() =>
                {
                    rotatingText.transform.parent = null;
                    Destroy(rotGO);
                    canInteract = true;
                });

            }

            pickedText = null;
            timer = 0f;
            timerStarted = false;
        }
        if (timerStarted)
            timer += Time.deltaTime;

        if (pickedText)
        {
            hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(Input.mousePosition));
            pickedText.transform.position = hit.point + offset;
            pickedText.transform.position += new Vector3(0, 0, -1);
            foreach (TextLoad tl in texts)
            {
                TextLoad pickedTextLoad = pickedText.GetComponent<TextLoad>();
                if (tl && tl != pickedTextLoad)
                {
                    if (Vector2.Distance(tl.transform.position, pickedText.transform.position) < minimumDistanceToSnap &&
                        pickedText.transform.rotation.x < 0.1f &&
                        tl.transform.rotation.x < 0.1f &&
                        (pickedTextLoad.allowedIDs.Any(x => tl.ID.Contains(x)) || 
                        tl.allowedIDs.Any(x => pickedTextLoad.ID.Contains(x))  
                        //tl.allowedIDs.Any(x => pickedTextLoad.allowedIDs.Contains(x)) ||
                        //pickedTextLoad.allowedIDs.Any(x => tl.allowedIDs.Contains(x)) 
                        ))
                    {
                        GetComponent<AudioSource>().PlayOneShot(stickSound);
                        pickedTextLoad.allowedIDs.AddRange(tl.allowedIDs);
                        pickedTextLoad.ID.AddRange(tl.ID);
                        tl.transform.parent = pickedText.transform;
                        tl.transform.DOLocalMove(Vector3.zero, 0.2f);
                        DestroyImmediate(tl);
                        if (FindObjectsOfType<TextLoad>().Length < 2)
                        {
                            GetComponent<AudioSource>().PlayOneShot(winSound);
                            FindObjectOfType<TextLoad>().transform.DOMove(new Vector3(0, 0, -1), 1f);
                            textExplanationGroup.DOFade(1, 1f).OnComplete(() => changeLevel = true);
                            foreach (MeshRenderer mr in FindObjectOfType<TextLoad>().GetComponentsInChildren<MeshRenderer>())
                            {
                                mr.material.SetTexture("_EmissionMap", null);
                                mr.material.DOColor(new Color(0.07058824f, 0.254902f, 0.3058824f), "_EmissionColor", 1f);
                                //mr.material.DOColor(new Color(0.07058824f, 0.254902f, 0.3058824f), 1f);
                            }
                        }

                    }
                }
            }
        }
        if (changeLevel && Input.GetMouseButtonDown(0) && FindObjectOfType<FlowController>().GameStarted)
        {
            FindObjectOfType<FlowController>().NextLevel();
            changeLevel = false;
        }
    }

    public void SetUpTexts()
    {
        texts = new List<TextLoad>(FindObjectsOfType<TextLoad>());
    }

    public void CleanState()
    {
        changeLevel = false;
        texts.Clear();
        canInteract = true;
    }

    public void PickUpTextTutorial(TextLoad text)
    {
        pickedText = text.gameObject;
    }
}
