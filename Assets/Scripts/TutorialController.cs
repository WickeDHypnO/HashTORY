﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TutorialController : MonoBehaviour
{
    [SerializeField]
    private TextSet tutorialSet = default;
    [SerializeField]
    private List<TextLoad> tutorialTexts = new List<TextLoad>();
    [SerializeField]
    private List<GameObject> disableObjects = new List<GameObject>();
    [SerializeField]
    private GameObject secondTitlePart = default;
    [SerializeField]
    private GameObject secondTitlePartSlideInPoint = default;
    [SerializeField]
    private CanvasGroup mainMenuCanvas = default;
    [SerializeField]
    private CanvasGroup explanationTextCanvas = default;
    private MeshRenderer hashPartRenderer = default;
    private bool doneTutorial = false;

    void OnEnable()
    {
        tutorialTexts[0].SetupText(
            tutorialSet.texts[0].textTexture,
            tutorialSet.texts[0].textColor,
            tutorialSet.texts[0].spriteImage,
            new List<int>() { 0 },
            new List<int>() { 1 });
        tutorialTexts[1].SetupText(
            tutorialSet.texts[1].textTexture,
            tutorialSet.texts[1].textColor,
            tutorialSet.texts[1].spriteImage,
            new List<int>() { 1 },
            new List<int>() { 0 });
        FindObjectOfType<PickUpText>().SetUpTexts();
        hashPartRenderer = tutorialTexts[0].GetComponent<MeshRenderer>();
    }

    private void Update()
    {
        if (FindObjectsOfType<TextLoad>().Length <= 1 && !doneTutorial)
        {
            var lastText = FindObjectOfType<TextLoad>().transform;
            foreach (GameObject go in disableObjects)
            {
                go.SetActive(false);
            }
            explanationTextCanvas.DOFade(0, 1f);
            secondTitlePart.transform.DOMove(secondTitlePartSlideInPoint.transform.position, 1f).OnComplete(() =>
            {
                mainMenuCanvas.gameObject.SetActive(true);
                mainMenuCanvas.DOFade(1f, 1f).OnComplete(() => lastText.parent = mainMenuCanvas.transform);
                secondTitlePart.transform.parent = mainMenuCanvas.transform;
                Destroy(FindObjectOfType<TextLoad>());
                enabled = false;
            });
            var endColor = hashPartRenderer.material.GetColor("_EmissionColor");
            tutorialTexts[1].GetComponent<MeshRenderer>().material.DOColor(endColor, "_EmissionColor", 1f);
            doneTutorial = true;
            PlayerPrefs.SetInt("finishedTutorial", 1);
        }
    }

    public void FinishedTutorialSetup()
    {
        mainMenuCanvas.gameObject.SetActive(true);
        mainMenuCanvas.DOFade(1f, 1f);
        explanationTextCanvas.alpha = 0f;
        foreach (TextLoad tl in FindObjectsOfType<TextLoad>())
        {
            tl.transform.parent = mainMenuCanvas.transform;
            tl.transform.position = new Vector3(0, 0, 0);
        }
        secondTitlePart.transform.parent = mainMenuCanvas.transform;
        foreach (GameObject go in disableObjects)
        {
            go.SetActive(false);
        }
        secondTitlePart.transform.DOMove(secondTitlePartSlideInPoint.transform.position, 1f).OnComplete(() =>
        {
            enabled = false;
            foreach (TextLoad tl in FindObjectsOfType<TextLoad>())
            {
                Destroy(tl);
            }
        });
        //var endColor = hashPartRenderer.material.GetColor("_EmissionColor");
        //tutorialTexts[1].GetComponent<MeshRenderer>().material.DOColor(endColor, "_EmissionColor", 1f);
        doneTutorial = true;
    }
}
