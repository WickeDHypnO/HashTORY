﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LogoCanvas : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup logoCanvas = default;
    [SerializeField]
    private CanvasGroup mainCanvas = default;

    void Start()
    {
        logoCanvas.DOFade(1, 1f).OnComplete(() =>
        {
            logoCanvas.DOFade(0, 1f);
            mainCanvas.DOFade(0, 1f).OnComplete(() =>
            {
                mainCanvas.gameObject.SetActive(false);
            });
        });
    }
}
