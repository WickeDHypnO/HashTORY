﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public struct TextSet
{
    public int LevelIDOnList;
    public List<TextElement> texts;
    public string textExplanation;
}

//TODO: Change into ScriptableObject in later stages
[System.Serializable]
public struct TextElement
{
    public List<int> ID;
    public List<int> allowedIDs;
    public Texture textTexture;
    public Sprite spriteImage;
    [ColorUsage(true)]
    public Color textColor;
    public Vector2 startPosition;
    public float startRotation;
}

public class TextSets : MonoBehaviour
{
    [SerializeField]
    private List<TextSet> sets = new List<TextSet>();
    [SerializeField]
    private GameObject textPrefab = default;
    private List<GameObject> createdPrefabs = new List<GameObject>();
    [SerializeField]
    private TextMeshProUGUI textExplanation = default;
    [SerializeField]
    private TextMeshProUGUI counter = default;

    [HideInInspector]
    public List<TextSet> Sets { get => sets; set => sets = value; }
    private void OnValidate()
    {
        foreach (TextSet set in sets)
        {
            List<TextElement> texts = set.texts;
            for (int i = 0; i < texts.Count; i++)
            {
                TextElement temp = texts[i];
                if(temp.startRotation == -90)
                {
                    temp.startRotation = 270;
                }
                //temp.ID.Clear();
                //temp.allowedIDs.Clear();
                //temp.ID.Add(i);
                //if (i != 0)
                //{
                //    temp.allowedIDs.Add(i - 1);
                //}
                //if (i != texts.Count - 1)
                //{
                //    temp.allowedIDs.Add(i + 1);
                //}
                //temp.textColor = new Color(1.5f, 1.5f, 1.5f);
                texts[i] = temp;
            }
        }
        // Sets.Sort((x, y) => x.LevelIDOnList.CompareTo(y.LevelIDOnList));
    }

    //TODO: Implement pooling in later stages
    public void LoadSet(int index)
    {
        if (index >= 0 && index < sets.Count)
        {
            if (FindObjectOfType<TextLoad>())
            {
                Destroy(FindObjectOfType<TextLoad>().gameObject);
            }
            createdPrefabs.Clear();
            foreach (TextElement t in sets[index].texts)
            {
                var text = Instantiate(textPrefab, new Vector3(t.startPosition.x, t.startPosition.y, -1), Quaternion.Euler(-270, -180, 0));
                text.GetComponent<TextLoad>().SetupText(t.textTexture, t.textColor, t.spriteImage, t.ID, t.allowedIDs);
                text.transform.Rotate(new Vector3(0, 0, -t.startRotation), Space.World);
            }
            textExplanation.text = sets[index].textExplanation;
            counter.text = index + 1 + "/" + sets.Count;
        }
        else
        {
            if (FindObjectOfType<TextLoad>())
            {
                Destroy(FindObjectOfType<TextLoad>().gameObject);
            }
            createdPrefabs.Clear();
            FindObjectOfType<FlowController>().GoToCredits();
        }
        FindObjectOfType<PickUpText>().SetUpTexts();
    }

    public void CleanUp()
    {
        foreach(TextLoad tl in FindObjectsOfType<TextLoad>())
        {
            Destroy(tl.gameObject);
        }
    }

    public bool CheckIfCredits(int index)
    {
        return index == sets.Count;
    }
}
