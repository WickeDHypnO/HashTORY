﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class FlowController : MonoBehaviour
{
    [SerializeField]
    private GameObject mainMenu = default;
    [SerializeField]
    private GameObject creditsScreen = default;
    [SerializeField]
    private TextSets sets = default;
    [SerializeField]
    private CanvasGroup fade = default;
    [SerializeField]
    private AudioSource mainAudio = default;
    [SerializeField]
    private int currentLevelIndex = 0;
    [SerializeField]
    private GameObject gameCanvas = default;
    [SerializeField]
    private GameObject continueButton = default;
    [SerializeField]
    private TextMeshProUGUI nextLevel = default;
    [SerializeField]
    private GameObject creditsMenuScreen = default;


    private bool continueGame = false;
    public bool GameStarted { get; set; }
    private void Start()
    {
        int finishedTutorial = PlayerPrefs.GetInt("finishedTutorial", 0);
        if (finishedTutorial != 0)
        {
            FindObjectOfType<TutorialController>().FinishedTutorialSetup();
        }
        int startedLevel = PlayerPrefs.GetInt("startedLevel", 0);
        if (startedLevel != 0)
        {
            continueButton.SetActive(true);
        }
    }

    public void ContinueGame()
    {
        continueGame = true;
        GoToGame();
    }

    public void GoToGame()
    {
        fade.GetComponent<GraphicRaycaster>().enabled = true;
        GameStarted = true;
        FindObjectOfType<PickUpText>().CleanState();

        fade.DOFade(1, 0.5f).OnComplete(() =>
        {
            mainMenu.SetActive(false);
            StartGame();
        });
    }

    private void StartGame()
    {
        if (continueGame)
        {
           currentLevelIndex = PlayerPrefs.GetInt("startedLevel", 0);
        }
        else
        {
           currentLevelIndex = 0;
        }
        nextLevel.gameObject.SetActive(true);
        FindObjectOfType<PickUpText>().textExplanationGroup.alpha = 0;
        fade.GetComponent<GraphicRaycaster>().enabled = false;
        //TODO: Add starting game music
        sets.LoadSet(currentLevelIndex);
        fade.DOFade(0, 0.5f);
        gameCanvas.SetActive(true);
    }

    public void NextLevel()
    {
        currentLevelIndex++;
        continueGame = true;
        PlayerPrefs.SetInt("startedLevel", currentLevelIndex);
        fade.GetComponent<GraphicRaycaster>().enabled = true;
        if (FindObjectOfType<TextSets>().CheckIfCredits(currentLevelIndex))
        {
            GoToCredits();
        }
        else
        {
            Debug.Log("testsets");
            fade.DOFade(1, 0.5f).OnComplete(() =>
            {
                StartGame();
            });
        }
    }

    private void Update()
    {
        if (GameStarted)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !FindObjectOfType<PickUpText>().changeLevel)
            {
                GoToMenu();
            }
        }
    }

    public void GoToMenu()
    {
        continueGame = false;
        fade.GetComponent<GraphicRaycaster>().enabled = true;
        fade.DOFade(1, 0.5f).OnComplete(() =>
        {
            if (creditsScreen.activeInHierarchy)
                creditsScreen.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() => creditsScreen.SetActive(false));
            else if (creditsMenuScreen.activeInHierarchy)
                creditsMenuScreen.GetComponent<CanvasGroup>().DOFade(0, 0.5f).OnComplete(() => creditsMenuScreen.SetActive(false));
            creditsMenuScreen.SetActive(false);
            creditsScreen.SetActive(false);
            mainMenu.SetActive(true);
            fade.DOFade(0, 0.5f).OnComplete(() => fade.GetComponent<GraphicRaycaster>().enabled = false);
            FindObjectOfType<PickUpText>().textExplanationGroup.alpha = 0;
            gameCanvas.SetActive(false);
            FindObjectOfType<TextSets>().CleanUp();
        });
    }

    public void GoToOptions()
    {
        //TODO: Add options screen
    }

    public void GoToCreditsFromMenu()
    {
        fade.GetComponent<GraphicRaycaster>().enabled = true;
        fade.DOFade(1, 0.5f).OnComplete(() =>
        {
            gameCanvas.SetActive(false);
            FindObjectOfType<TextSets>().CleanUp();
            FindObjectOfType<PickUpText>().textExplanationGroup.alpha = 0;
            mainMenu.SetActive(false);
            fade.DOFade(0, 0.5f).OnComplete(() => fade.GetComponent<GraphicRaycaster>().enabled = false);
            creditsMenuScreen.SetActive(true);
            creditsMenuScreen.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
        });
    }
    public void GoToCredits()
    {
        fade.GetComponent<GraphicRaycaster>().enabled = true;
        fade.DOFade(1, 0.5f).OnComplete(() =>
        {
            gameCanvas.SetActive(false);
            FindObjectOfType<TextSets>().CleanUp();
            FindObjectOfType<PickUpText>().textExplanationGroup.alpha = 0;
            mainMenu.SetActive(false);
            fade.DOFade(0, 0.5f).OnComplete(() => fade.GetComponent<GraphicRaycaster>().enabled = false);
            creditsScreen.SetActive(true);
            creditsScreen.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
        });
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    [ContextMenu("Clean up player prefs")]
    public void CleanUpPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
