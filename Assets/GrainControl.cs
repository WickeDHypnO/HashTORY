﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrainControl : MonoBehaviour
{

    public float interval;
    public Vector2 movement;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.fixedDeltaTime;
        if(timer > interval)
        {
            timer = 0;
            float xValue = Random.Range(-movement.x, movement.x);
            float yValue = Random.Range(-movement.y, movement.y);
            int randomRotation = Random.Range(0, 359);
            transform.localPosition = new Vector3(xValue, yValue, 10);
            transform.localEulerAngles = new Vector3(0f, 0f, randomRotation);
        }
    }
}
